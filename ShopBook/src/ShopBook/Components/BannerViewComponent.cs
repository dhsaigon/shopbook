﻿using System.Linq;
using ShopBook.Models;
using Microsoft.AspNetCore.Mvc;

namespace ShopBook.Components
{
    public class BannerViewComponent: ViewComponent
    {
        private IProductRepository repository;
        public BannerViewComponent(IProductRepository repo)
        {
            repository = repo;
        }
        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            return View(repository.Products
            .Select(x => x.Category)
            .Distinct()
            .OrderBy(x => x));
        }
    }
}
