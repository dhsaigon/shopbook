﻿using Microsoft.AspNetCore.Mvc;
using ShopBook.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ShopBook.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IProductRepository repository;
        private readonly IHostingEnvironment _environment;
        public AdminController(IProductRepository repo, IHostingEnvironment environment)
        {
            repository = repo;
            _environment = environment;
        }
        public ViewResult Index() => View(repository.Products);

        public ViewResult Edit(int productId) =>View(repository.Products
                .FirstOrDefault(p => p.ProductID == productId));

        [HttpPost]
        public async Task<IActionResult> Edit(Product product, ICollection<IFormFile> files)
        {
            try
            {
                var uploads = Path.Combine(_environment.WebRootPath, "images");
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);

                            product.files = file.FileName;
                        }
                    }
                }

                ViewBag.Message = "File uploaded successfully";
            }
            catch (Exception ex)
            {
                ViewBag.Message = string.Format("Error: {0}", ex.Message);
            }

            if (ModelState.IsValid)
            {
                repository.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            }
            else
            {
                // there is something wrong with the data values
                return View(product);
            }
        }

        public ViewResult Create() => View("Edit", new Product());

        [HttpPost]
        public IActionResult Delete(int productId)
        {
            Product deletedProduct = repository.DeleteProduct(productId);
            if (deletedProduct != null)
            {
                TempData["message"] = $"{deletedProduct.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
    }
}